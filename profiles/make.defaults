# Copyright 2010 Bo Ørsted Andresen
# Distributed under the terms of the GNU General Public License v2

CONFIG_PROTECT="/etc"
CONFIG_PROTECT_MASK="/etc/env.d /etc/xdg"

# Prevent Python from trying to violate the sandbox when importing a
# module that doesn't already have suitable bytecode written
PYTHONDONTWRITEBYTECODE=1

MULTIBUILD_CLASSES="LUA_ABIS PHP_ABIS PYTHON_ABIS RUBY_ABIS POSTGRESQL_SERVERS"

CROSS_COMPILE_TOOLS="AR:ar AS:as CC:cc CPP:cpp CXX:c++ FORTRAN:gfortran LD:ld NM:nm OBJCOPY:objcopy OBJDUMP:objdump PKG_CONFIG:pkg-config RANLIB:ranlib READELF:readelf"
CROSS_COMPILE_FLAGS="CFLAGS CPPFLAGS:CFLAGS CXXFLAGS CXXCPPFLAGS:CXXFLAGS LDFLAGS FCFLAGS"

# MULTIBUILD_LUA_ABIS_TARGETS below need to be in sync with
# LUA_AVAILABLE_ABIS in exlibs/lua.exlib.
MULTIBUILD_LUA_ABIS_TARGETS="5.1 5.2 5.3 5.4"
MULTIBUILD_LUA_ABIS_DEFAULT_TARGET="5.1"
MULTIBUILD_LUA_ABIS_LOCKED_VARS="LUA"
MULTIBUILD_LUA_ABIS_REQUIRED_VARS="LUA"
MULTIBUILD_LUA_ABIS_5_4_PROFILE_LUA="lua5.4"
MULTIBUILD_LUA_ABIS_5_3_PROFILE_LUA="lua5.3"
MULTIBUILD_LUA_ABIS_5_2_PROFILE_LUA="lua5.2"
MULTIBUILD_LUA_ABIS_5_1_PROFILE_LUA="lua5.1"

# MULTIBUILD_PYTHON_ABIS_TARGETS below need to be in sync with
# PYTHON_AVAILABLE_ABIS in exlibs/python.exlib.
MULTIBUILD_PYTHON_ABIS_TARGETS="2.7 3.6 3.7 3.8 3.9 3.10"
MULTIBUILD_PYTHON_ABIS_LOCKED_VARS="PYTHON"
MULTIBUILD_PYTHON_ABIS_REQUIRED_VARS="PYTHON"
MULTIBUILD_PYTHON_ABIS_3_10_PROFILE_PYTHON="python3.10"
MULTIBUILD_PYTHON_ABIS_3_9_PROFILE_PYTHON="python3.9"
MULTIBUILD_PYTHON_ABIS_3_8_PROFILE_PYTHON="python3.8"
MULTIBUILD_PYTHON_ABIS_3_7_PROFILE_PYTHON="python3.7"
MULTIBUILD_PYTHON_ABIS_3_6_PROFILE_PYTHON="python3.6"
MULTIBUILD_PYTHON_ABIS_2_7_PROFILE_PYTHON="python2.7"

# this is kept around to ensure old env can be sourced when replacing packages
MULTIBUILD_PYTHON_ABIS_3_4_PROFILE_PYTHON="python3.5"
MULTIBUILD_PYTHON_ABIS_3_4_PROFILE_PYTHON="python3.4"
MULTIBUILD_PYTHON_ABIS_3_3_PROFILE_PYTHON="python3.3"
MULTIBUILD_PYTHON_ABIS_3_2_PROFILE_PYTHON="python3.2"
MULTIBUILD_PYTHON_ABIS_3_1_PROFILE_PYTHON="python3.1"

# MULTIBUILD_RUBY_ABIS_TARGETS below need to be in sync with
# RUBY_AVAILABLE_ABIS in exlibs/ruby.exlib.
MULTIBUILD_RUBY_ABIS_TARGETS="2.6 2.7 3.0"
MULTIBUILD_RUBY_ABIS_DEFAULT_TARGET="2.6"
MULTIBUILD_RUBY_ABIS_LOCKED_VARS="RUBY"
MULTIBUILD_RUBY_ABIS_REQUIRED_VARS="RUBY"
MULTIBUILD_RUBY_ABIS_3_0_PROFILE_GEM="gem3.0"
MULTIBUILD_RUBY_ABIS_3_0_PROFILE_RUBY="ruby3.0"
MULTIBUILD_RUBY_ABIS_2_7_PROFILE_GEM="gem2.7"
MULTIBUILD_RUBY_ABIS_2_7_PROFILE_RUBY="ruby2.7"
MULTIBUILD_RUBY_ABIS_2_6_PROFILE_GEM="gem2.6"
MULTIBUILD_RUBY_ABIS_2_6_PROFILE_RUBY="ruby2.6"

# MULTIBUILD_POSTGRESQL_ABIS_TARGETS below need to be in sync with
# POSTGRESQL_AVAILABLE_ABIS in exlibs/postgresql.exlib
MULTIBUILD_POSTGRESQL_SERVERS_TARGETS="9.6 10 11 12 13"
MULTIBUILD_POSTGRESQL_SERVERS_LOCKED_VARS="PG_CONFIG"


XDG_RUNTIME_DIR=${TEMP}

# (C|CXX)FLAGS which are to be used by default should be listed here
# Don't use generic flags, use -march=native and such whenever possible
armv7_unknown_linux_gnueabi_CFLAGS="-march=native -O2 -pipe"
armv7_unknown_linux_gnueabi_CXXFLAGS="-march=native -O2 -pipe"
armv7_unknown_linux_gnueabihf_CFLAGS="-march=native -mcpu=native -mfloat-abi=hard -O2 -pipe"
armv7_unknown_linux_gnueabihf_CXXFLAGS="-march=native -mcpu=native -mfloat-abi=hard -O2 -pipe"
i686_pc_linux_gnu_CFLAGS="-march=native -O2 -pipe"
i686_pc_linux_gnu_CXXFLAGS="-march=native -O2 -pipe"
i686_pc_linux_musl_CFLAGS="-march=native -O2 -pipe"
i686_pc_linux_musl_CXXFLAGS="-march=native -O2 -pipe"
x86_64_pc_linux_gnu_CFLAGS="-march=native -O2 -pipe"
x86_64_pc_linux_gnu_CXXFLAGS="-march=native -O2 -pipe"
x86_64_pc_linux_musl_CFLAGS="-march=native -O2 -pipe"
x86_64_pc_linux_musl_CXXFLAGS="-march=native -O2 -pipe"

LDFLAGS="-Wl,-O1 -Wl,--as-needed"
