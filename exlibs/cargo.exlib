# Copyright 2016 Marc-Antoine Perennou
# Distributed under the terms of the GNU General Public License v2

myexparam crate=${PN}
myexparam rust_minimum_version=1.36.0
myexparam channel=all
myexparam -b disable_default_features=false
myexparam -b with_opt=false

if exparam -b with_opt; then
    myexparam option_name=rust
    exparam -v OPTION_NAME option_name
fi

exparam -v CRATE crate
exparam -v RUST_CHANNEL channel

HOMEPAGE="https://crates.io/crates/${CRATE}"
if [[ $(type -t scm_src_unpack) == function ]]; then
   DOWNLOADS=""
else
   DOWNLOADS="https://crates.io/api/v1/crates/${CRATE}/${PV}/download -> ${PNV}.tar.gz"
fi

WORK="${WORKBASE}/${CRATE}-${PV}"

if [[ "${CATEGORY}/${PN}" != "dev-lang/rust" ]]; then
    rust_slot_requirement() {
        case ${RUST_CHANNEL} in
            stable|beta|nightly)
                echo ${RUST_CHANNEL}
                ;;
            all)
                echo "*"
                ;;
            same)
                echo "="
                ;;
            *)
                eerror "Invalid channel: ${RUST_CHANNEL}"
        esac
    }

    rust_dep="dev-lang/rust:$(rust_slot_requirement)[>=$(exparam rust_minimum_version)]"
    if exparam -b with_opt ; then
        rust_dep="$(exparam option_name)? ( ${rust_dep} )"
    fi

    DEPENDENCIES="
        build:
            ${rust_dep}
    "
fi

export_exlib_phases src_unpack src_configure src_compile src_test src_install

export CARGO_HOME="${FETCHEDDIR}/cargo-home/"

cargo_get_tool() {
    local tool=${1}

    case ${RUST_CHANNEL} in
        stable|beta|nightly)
            echo ${tool}-${RUST_CHANNEL}
            ;;
        *)
            echo ${tool}
            ;;
    esac
}

ecargo() {
    local cmd="${1}"
    local params=()
    local features=()
    local feature
    local feat

    shift

    if [[ -n "${OPTION_NAME}" ]] && ! option ${OPTION_NAME}; then
        return
    fi

    if [[ "${cmd}" != "fetch" ]]; then
        if exparam -b disable_default_features; then
            params+=( --no-default-features )
        fi

        for feature in "${ECARGO_FEATURES[@]}"; do
            features+=( ${feature} )
        done
        for feature in "${ECARGO_FEATURE_ENABLES[@]}"; do
            feat=$(option ${feature})
            if [[ -n "${feat}" ]]; then
                features+=( ${feat} )
            fi
        done

        if [[ ${#features[@]} != 0 ]]; then
            params+=( --features "${features[*]}" )
        fi
    fi

    esandbox allow "${CARGO_HOME}"
    RUSTC="${RUSTC:-$(cargo_get_tool rustc)}" RUSTDOC="${RUSTDOC:-$(cargo_get_tool rustdoc)}" edo "${CARGO:-$(cargo_get_tool cargo)}" "${cmd}" "${params[@]}" "${@}"
    esandbox disallow "${CARGO_HOME}"
}

ecargo_fetch() {
    # TODO(keruspe): nuke that when we handle system-wide deps properly
    # This issue would make things better already: https://github.com/rust-lang/cargo/issues/2998

    # Fetch dependencies before further offline processing
    esandbox disable_net
    ecargo fetch
    esandbox enable_net
}

ecargo_config() {
    # Tell cargo to be verbose and respect EXJOBS
    edo mkdir -p .cargo
    cat >> .cargo/config <<EOF
[term]
verbose = true
[build]
jobs = ${EXJOBS:-1}
[net]
offline = true
EOF
}

cargo_src_unpack() {
    if [[ $(type -t scm_src_unpack) == function ]]; then
        scm_src_unpack
    else
        default
    fi

    edo cd "${WORK}"

    ecargo_fetch
}

cargo_src_configure() {
    ecargo_config
}

cargo_src_compile() {
    ecargo build --release --frozen
}

cargo_src_test() {
    ecargo test --release --frozen
}

ecargo_install() {
    ecargo install --path=. --root="${IMAGE}/usr/$(exhost --target)" --frozen
    nonfatal edo rm "${IMAGE}/usr/$(exhost --target)/.crates.toml"
    nonfatal edo rm "${IMAGE}/usr/$(exhost --target)/.crates2.json"
    # [[ -d man/ ]] && doman man/*
    [[ -d man/ ]] && find man -type f -exec doman {} \;
}

cargo_src_install() {
    ecargo_install
    emagicdocs
}

