# Copyright 2019 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2012 Saleem Abdulrasool <compnerd@compnerd.org>
# Copyright 2017 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Public License v2

if ever at_least 11; then
    require llvm-project [ pn="libcxxabi" check_target="check-cxxabi" asserts=true rtlib=true ]
else
    require llvm-project [ pn="libcxxabi" check_target="check-libcxxabi" asserts=true rtlib=true ]
fi

export_exlib_phases src_prepare src_install src_test

SUMMARY="A new implementation of low level support for a standard C++ library"

DEPENDENCIES="
    post:
        sys-libs/libc++[~${PV}]
"
CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DLIBCXX_ENABLE_SHARED:BOOL=ON
    -DLIBCXXABI_ENABLE_EXCEPTIONS:BOOL=ON
    -DLIBCXXABI_ENABLE_PIC:BOOL=ON
    -DLIBCXXABI_ENABLE_SHARED:BOOL=ON
    -DLIBCXXABI_ENABLE_STATIC:BOOL=ON
    -DLIBCXXABI_ENABLE_STATIC_UNWINDER:BOOL=OFF
    -DLIBCXXABI_ENABLE_THREADS:BOOL=ON
    -DLIBCXXABI_ENABLE_WERROR:BOOL=OFF
    -DLIBCXXABI_INCLUDE_TESTS:BOOL=ON
    -DLIBCXXABI_INSTALL_LIBRARY:BOOL=ON
    -DLIBCXXABI_INSTALL_SHARED_LIBRARY:BOOL=ON
    -DLIBCXXABI_INSTALL_STATIC_LIBRARY:BOOL=ON
    -DLIBCXXABI_LIBCXX_INCLUDES:PATH="${WORKBASE}/llvm-project/libcxx/include"
    -DLIBCXXABI_STATICALLY_LINK_UNWINDER_IN_STATIC_LIBRARY:BOOL=ON
)

CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'asserts LIBCXXABI_ENABLE_ASSERTIONS'
    'providers:compiler-rt LIBCXXABI_USE_COMPILER_RT'
    'providers:compiler-rt LIBCXXABI_USE_LLVM_UNWINDER'
)

libc++abi_src_prepare() {
    default

    edo ln -s "${WORKBASE}"/llvm-project/{,llvm/projects/}libcxx
}

libc++abi_src_install() {
    cmake_src_install

    # install headers
    insinto /usr/$(exhost --target)/include/libc++abi
    doins "${CMAKE_SOURCE}"/include/*
}

libc++abi_src_test() {
    # Avoid dependency loop between libc++ and libc++abi
    if has_version sys-libs/libc++[~${PV}]; then
        llvm-project_src_test
    else
        ewarn "Test dependencies are not yet installed, skipping tests"
    fi
}

