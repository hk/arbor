# Copyright 2009-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Copyright 2014 Julien Pivotto <roidelapluie@inuits.eu>
# Distributed under the terms of the GNU General Public License v2

require libnetfilter
require python [ blacklist=2 multibuild=false with_opt=true ]

SUMMARY="nftables user space utility"
DESCRIPTION="
nftables is the project that aims to replace the existing {ip,ip6,arp,eb}tables
framework. Basically, this project provides a new packet filtering framework, a
new userspace utility and also a compatibility layer for {ip,ip6}tables. nftables
is built upon the building blocks of the Netfilter infrastructure such as the
existing hooks, the connection tracking system, the userspace queueing component
and the logging subsystem.
"

LICENCES="GPL-2"
MYOPTIONS="
    debug
    json [[ description = [ Enable JSON output support ] ]]
"

DEPENDENCIES="
    build:
        app-doc/asciidoc
        sys-devel/bison
        sys-devel/flex
    build+run:
        dev-libs/gmp:=
        net-libs/libmnl[>=1.0.4]
        net-libs/libnftnl[>=1.2.0]
        sys-libs/readline:=
        json? ( dev-libs/jansson )
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-man-doc
    --with-cli=readline
    --with-python-bin=${PYTHON}
    --without-mini-gmp
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    json
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    debug
    python
)

