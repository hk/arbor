# Copyright 2008 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=${PN}-software tag=version-${PV} ] cmake

SUMMARY="Software-based implementation of the codec specified in the JPEG-2000 Part-1 standard"
HOMEPAGE+=" https://www.ece.uvic.ca/~frodo/${PN}"

LICENCES="Jasper-2.0"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    opengl
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build+run:
        opengl? (
            dev-libs/libglvnd
            x11-dri/freeglut
            x11-dri/glu
        )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DCMAKE_BUILD_TYPE:STRING=Release
    -DCMAKE_DISABLE_FIND_PACKAGE_Doxygen:BOOL=TRUE
    -DCMAKE_DISABLE_FIND_PACKAGE_LATEX:BOOL=TRUE
    -DCMAKE_INSTALL_DOCDIR=/usr/share/doc/${PNVR}
    -DJAS_ENABLE_32BIT:BOOL=FALSE
    -DJAS_ENABLE_ASAN:BOOL=FALSE
    -DJAS_ENABLE_AUTOMATIC_DEPENDENCIES:BOOL=FALSE
    -DJAS_ENABLE_DANGEROUS_INTERNAL_TESTING_MODE:BOOL=FALSE
    -DJAS_ENABLE_DOC:BOOL=FALSE
    -DJAS_ENABLE_FUZZER:BOOL=FALSE
    -DJAS_ENABLE_HIDDEN:BOOL=FALSE
    -DJAS_ENABLE_LIBJPEG:BOOL=TRUE
    -DJAS_ENABLE_LSAN:BOOL=FALSE
    -DJAS_ENABLE_MSAN:BOOL=FALSE
    -DJAS_ENABLE_PROGRAMS:BOOL=TRUE
    -DJAS_ENABLE_SHARED:BOOL=TRUE
    -DJAS_ENABLE_USAN:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'opengl JAS_ENABLE_OPENGL'
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

