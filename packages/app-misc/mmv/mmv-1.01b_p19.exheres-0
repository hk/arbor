# Copyright 2008-2017 Wulf C. Krueger
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'mmv-1.01b_p14.ebuild' from Gentoo, which is:
#       Copyright 1999-2008 Gentoo Foundation

DEB_PATCH_VER=${PV#*_p}
MY_PNV=${PN}_${PV%_p*}

SUMMARY="Move/copy/append/link multiple files according to a set of wildcard patterns."
DESCRIPTION="
mmv is a program to move/copy/append/link multiple files according to a set of
wildcard patterns. This multiple action is performed safely, i.e. without any
unexpected deletion of files due to collisions of target names with existing
filenames or with other target names.
"
HOMEPAGE="http://packages.debian.org/unstable/utils/${PN}"
DOWNLOADS="mirror://debian/pool/main/m/${PN}/${MY_PNV}.orig.tar.gz"

UPSTREAM_CHANGELOG="http://packages.debian.org/changelogs/pool/main/m/${PN}/${MY_PNV}/changelog"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES=""

# All patches from https://sources.debian.net/src/mmv/1.01b-19/debian/patches/
#
# DEB_VER="1.01b-19"
# DEB_URL="https://sources.debian.net/data/main/m/mmv/${DEB_VER}/debian/patches/"
# for patch in $(wget ${DEB_URL}/series -O -); do
#   wget ${DEB_URL}/${patch} -O arbor/packages/app-misc/mmv/files/${DEB_VER}/${patch}
# done
#
DEFAULT_SRC_PREPARE_PATCHES=( "${FILES}"/${PV/_p/-}/ )

DEFAULT_SRC_COMPILE_PARAMS=(
    CC="${CC}"
    CFLAGS+="-D_FILE_OFFSET_BITS=64 -D_LARGEFILE_SOURCE -D_LARGEFILE64_SOURCE"
    LDFLAGS="${LDFLAGS}"
)

WORK=${WORKBASE}/${MY_PNV/_/-}.orig

src_install() {
    dobin mmv
    doman mmv.1
    dodoc ANNOUNCE
}

