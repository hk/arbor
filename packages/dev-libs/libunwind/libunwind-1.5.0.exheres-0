# Copyright 2012, 2014 Ali Polatel <alip@exherbo.org>
# Based in part upon libunwind-1.1.ebuild of Gentoo which is:
#   Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.16 1.15 1.13 ] ]

SUMMARY="C programming interface to determine the call-chain of a program"
DESCRIPTION="
The primary goal of this project is to define a portable and efficient C
programming interface (API) to determine the call-chain of a program. The API
additionally provides the means to manipulate the preserved (callee-saved) state
of each call-frame and to resume execution at any point in the call-chain
(non-local goto).
"
HOMEPAGE="http://www.nongnu.org/libunwind/"
DOWNLOADS="http://download.savannah.nongnu.org/releases/${PN}/${PNV}.tar.gz"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    debug [[ description = [ Enable debug-frame, minidebuginfo and other goodies ] ]]
    doc
"

DEPENDENCIES="
    build:
        doc? ( app-text/texlive-core [[ note = latex2man ]] )
    build+run:
        debug? ( app-arch/xz )
        sys-libs/zlib
        !sys-libs/llvm-libunwind [[
            description = [ Both packages provide a libunwind implementation ]
            resolution = uninstall-blocked-after
        ]]
"

# Some tests are known to fail on some platforms including x86, see
#  https://bugs.gentoo.org/show_bug.cgi?id=461958 and
#  https://lists.nongnu.org/archive/html/libunwind-devel/2013-03/msg00005.html
RESTRICT="test"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-tests-Use-correctly-prefixed-nm-binary.patch
    "${FILES}"/0002-tests-Remove-coredump-tests.patch
)

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-cxx-exceptions
    --enable-zlibdebuginfo
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    debug
    'debug minidebuginfo'
    'debug debug-frame'
    'doc documentation'
)
DEFAULT_SRC_CONFIGURE_TESTS=( '--enable-tests --disable-tests' )

