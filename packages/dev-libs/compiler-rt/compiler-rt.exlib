# Copyright 2019 Marc-Antoine Perennou <keruspe@exherbo.org>
# Copyright 2015 Niels Ole Salscheider <niels_ole@salscheider-online.de>
# Copyright 2012 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require llvm-project [ slotted=true rtlib=true stdlib=true ]

export_exlib_phases src_prepare src_configure

SUMMARY="Sanitizer and profiling runtimes for clang"

MYOPTIONS="
    sanitizers [[ description = [ Build runtime libraries for sanitizer instrumentation ] ]]
    xray [[ description = [ Build support for XRay instrumentation ] ]]

    ( libc: musl )
"

DEPENDENCIES="
    build+run:
        dev-lang/clang:${SLOT}
        dev-lang/llvm:${SLOT}
        !dev-libs/compiler-rt:0 [[
            description = [ Old, unslotted compiler-rt ]
            resolution = upgrade-blocked-before
        ]]
        libc:musl? (
            sanitizers? ( dev-libs/musl-obstack )
            xray? ( dev-libs/libexecinfo )
        )
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    '-DCOMPILER_RT_BUILD_BUILTINS:BOOL=ON'
    '-DCOMPILER_RT_BUILD_CRT:BOOL=ON'
    '-DCOMPILER_RT_BUILD_LIBFUZZER:BOOL=OFF'
    '-DCOMPILER_RT_BUILD_PROFILE:BOOL=ON'
    '-DCOMPILER_RT_BUILD_XRAY_NO_PREINIT:BOOL=OFF'
    '-DCOMPILER_RT_CRT_USE_EH_FRAME_REGISTRY:BOOL=ON'
    '-DCOMPILER_RT_DEBUG:BOOL=OFF'
    "-DCOMPILER_RT_DEFAULT_TARGET_TRIPLE:STRING=$(exhost --target)"
    '-DCOMPILER_RT_ENABLE_WERROR:BOOL=OFF'
    '-DCOMPILER_RT_EXCLUDE_ATOMIC_BUILTIN:BOOL=ON'
    '-DCOMPILER_RT_HWASAN_WITH_INTERCEPTORS:BOOL=ON'
    '-DCOMPILER_RT_INCLUDE_TESTS:BOOL=ON'
    '-DCOMPILER_RT_SANITIZERS_TO_BUILD:STRING=all'
    '-DSANITIZER_ALLOW_CXXABI:BOOL=ON'
    '-DSANITIZER_USE_STATIC_LLVM_UNWINDER:BOOL=ON'
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'sanitizers COMPILER_RT_BUILD_SANITIZERS'
    'xray COMPILER_RT_BUILD_XRAY'
)

RESTRICT="test"

compiler-rt_src_prepare() {
    edo pushd "${CMAKE_SOURCE}"
    default

    # Use our prefixed NM
    edo sed -e "s/\['nm', library\]/\['${NM}', library\]/" \
            -i lib/sanitizer_common/scripts/gen_dynamic_list.py
    edo popd
}

compiler-rt_src_configure() {
    export PATH="${LLVM_PREFIX}/bin/:${PATH}"

    local clang_exe=$(llvm-config --bindir)/clang
    local resource_dir=$(${clang_exe} -print-resource-dir)
    local args=( -DCOMPILER_RT_INSTALL_PATH=${resource_dir} )

    option providers:compiler-rt && args+=( -DCOMPILER_RT_USE_BUILTINS_LIBRARY:BOOL=ON )

    if option providers:libc++; then
        args+=(
            -DCOMPILER_RT_USE_LIBCXX:BOOL=ON
            -DSANITIZER_CXX_ABI:STRING="libc++"
        )
    else
        args+=(
            -DCOMPILER_RT_USE_LIBCXX:BOOL=OFF
            -DSANITIZER_CXX_ABI="libstdc++"
        )
    fi


    cmake_src_configure "${args[@]}"
}

