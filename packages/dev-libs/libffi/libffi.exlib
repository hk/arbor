# Copyright 2008, 2010 Ingmar Vanhassel <ingmar@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ release=v${PV} suffix=tar.gz ]
require alternatives

export_exlib_phases src_install

SUMMARY="A portable foreign function interface library"
DESCRIPTION="
Compilers for high level languages generate code that follows certain conventions. These conventions are necessary,
in part, for separate compilation to work. One such convention is the \"calling convention\".
The \"calling convention\" is a set of assumptions made by the compiler about where function arguments will be found on entry to a function.
A \"calling convention\" also specifies where the return value for a function is found.

Some programs may not know at the time of compilation what arguments are to be passed to a function.
For instance, an interpreter may be told at run-time about the number and types of arguments used to call a given function.
Libffi can be used in such programs to provide a bridge from the interpreter program to compiled code.

The libffi library provides a portable, high level programming interface to various calling conventions.
This allows a programmer to call any function specified by a call interface description at run-time.

FFI stands for Foreign Function Interface.
A foreign function interface is the popular name for the interface that allows code written in one language to call code written in another language.
The libffi library really only provides the lowest, machine dependent layer of a fully featured foreign function interface.
A layer must exist above libffi that handles type conversions for values passed between the two languages.
"
HOMEPAGE="https://sourceware.org/${PN} ${HOMEPAGE}"

LICENCES="MIT"
MYOPTIONS="
    debug
    parts: development documentation libraries
"

DEPENDENCIES="
    run:
        !dev-libs/libffi:0[<3.2.1-r2] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
    test:
        dev-util/dejagnu
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --disable-docs
    --disable-multi-os-directory
    --disable-static
)
DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( debug )

libffi_src_install() {
    default

    local arch_dependent_alternatives=(
        /usr/$(exhost --target)/lib/libffi.so          ${PN}-${SLOT}.so
        /usr/$(exhost --target)/lib/libffi.la          ${PN}-${SLOT}.la
        /usr/$(exhost --target)/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )
    local arch_independent_alternatives=(
        /usr/share/man/man3/ffi_prep_cif_var{,-${SLOT}}.3
    )

    if ever at_least 3.3 ; then
        arch_dependent_alternatives+=(
            /usr/$(exhost --target)/include/ffi.h          ffi.h-${SLOT}
            /usr/$(exhost --target)/include/ffitarget.h    ffitarget.h-${SLOT}
        )
    fi

    alternatives_for _$(exhost --target)_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
    alternatives_for _${PN} ${SLOT} ${SLOT} "${arch_independent_alternatives[@]}"

    # FIXME: our gcc installs it's own copy of libffi instead of using the system one
    edo rm "${IMAGE}"/usr/share/man/man3/{ffi,ffi_call,ffi_prep_cif}.3

    expart libraries /usr/$(exhost --target)/lib
    expart development /usr/$(exhost --target)/lib/${PNV}/include
    expart development /usr/$(exhost --target)/lib/pkgconfig
    expart documentation /usr/share/{doc,man,info}
}

