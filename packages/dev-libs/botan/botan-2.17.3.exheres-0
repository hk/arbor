# Copyright 2009-2012 Pierre Lejeune <superheron@gmail.com>
# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require botan python [ blacklist=none multibuild=false ]
require toolchain-funcs

DOWNLOADS="${HOMEPAGE}releases/${MY_PNV}.tar.xz"

# release_so_abi_rev in src/build-data/version.txt
SLOT="17"
PLATFORMS="~amd64"

src_configure() {
    local target=$(exhost --target)

    if cxx-is-gcc;then
        compiler="gcc"
    elif cxx-is-clang;then
        compiler="clang"
    else
        die "Unknown compiler ${CXX}; you will need to add a check for it to the botan exheres"
        # Available options upstream: "clang ekopath gcc hpcc icc msvc pgi sunstudio xlc"
    fi

    local params=(
        --distribution-info=Exherbo
        --prefix=/usr/$(exhost --target)
        --cc=${compiler}
        --docdir=/usr/share/doc
        --cpu=${target%%-*}
        --os=linux
        --disable-static-library
        --enable-shared
        --system-cert-bundle=/etc/ssl/certs/ca-certificates.crt
        --with-bzip2
        --with-openssl
        --with-lzma
        --with-zlib
        --with-pkg-config
        --with-python-version=$(python_get_abi)
        --without-commoncrypto
        --without-doxygen
        --without-tpm
        $(option_with boost)
        $(option_with doc documentation)
        $(option_with doc sphinx)
        $(option_with sqlite sqlite3)
    )

    if option platform:x86 && option !x86_cpu_features:sse2 ; then
        params+=( --disable-sse2 )
    fi

    for feature in ssse3 sse4.1 sse4.2 avx2 ; do
        if option !amd64_cpu_features:${feature} && option !x86_cpu_features:${feature} ; then
            params+=( --disable-${feature} )
        fi
    done
    edo ./configure.py "${params[@]}"
}

src_test() {
    LD_LIBRARY_PATH="${WORK}" edo ./botan-test
}

