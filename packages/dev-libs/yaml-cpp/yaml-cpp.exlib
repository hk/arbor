# Copyright 2009 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require github [ user=jbeder tag=${PNV} ] cmake
require alternatives

export_exlib_phases src_install

SUMMARY="A YAML parser and emitter in C++ written around the YAML 1.2 spec. It's based on (but not a wrapper to) the C library libyaml"

LICENCES="MIT"
MYOPTIONS=""

DEPENDENCIES="
    run:
        !dev-libs/yaml-cpp:0[<0.6.3-r1] [[
            description = [ Alternatives conflict ]
            resolution = upgrade-blocked-before
        ]]
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DYAML_BUILD_SHARED_LIBS:BOOL=TRUE
    -DYAML_CPP_BUILD_TOOLS:BOOL=TRUE
    -DYAML_CPP_INSTALL:BOOL=TRUE
)

CMAKE_SRC_CONFIGURE_TESTS=(
    '-DYAML_CPP_BUILD_TESTS:BOOL=TRUE -DYAML_CPP_BUILD_TESTS:BOOL=FALSE'
)

if ever at_least 0.7.0; then
    DEPENDENCIES+="
        test:
            dev-cpp/gtest[googlemock]
    "
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DYAML_CPP_BUILD_CONTRIB:BOOL=TRUE
    )
else
    CMAKE_SRC_CONFIGURE_PARAMS+=(
        -DBUILD_GMOCK:BOOL=FALSE
        -DBUILD_GTEST:BOOL=FALSE
    )
fi

yaml-cpp_src_install() {
    local arch_dependent_alternatives=() host=$(exhost --target)

    default

    arch_dependent_alternatives+=(
        /usr/${host}/include/${PN}          ${PN}-${SLOT}
        /usr/${host}/lib/lib${PN}.so        lib${PN}-${SLOT}.so
        /usr/${host}/lib/cmake/${PN}        ${PN}-${SLOT}
        /usr/${host}/lib/pkgconfig/${PN}.pc ${PN}-${SLOT}.pc
    )

    alternatives_for _${host}_${PN} ${SLOT} ${SLOT} "${arch_dependent_alternatives[@]}"
}

