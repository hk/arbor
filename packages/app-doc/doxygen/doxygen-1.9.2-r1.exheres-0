# Copyright 2008, 2009 Ingmar Vanhassel <ingmar@exherbo.org>
# Copyright 2010 Cecil Curry <leycec@gmail.com>
# Copyright 2015-2018 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'doxygen-1.5.5.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation.

require cmake utf8-locale

SUMMARY="Documentation system for C++, C, Java, Objective-C, Python, IDL, and other languages"
HOMEPAGE="http://www.doxygen.nl"
DOWNLOADS="${HOMEPAGE}/files/${PNV}.src.tar.gz
    doc? ( ${HOMEPAGE}/files/${PN}_manual-${PV}.pdf.zip )"

UPSTREAM_DOCUMENTATION="
${HOMEPAGE}/manual/faq.html [[ description = [ Doxygen FAQ ] lang = en ]]
${HOMEPAGE}/manual/index.html [[ description = [ Doxygen Manual ] lang = en ]]
"
UPSTREAM_RELEASE_NOTES="${HOMEPAGE}/manual/changelog.html"

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS="
    doc
    dot      [[ description = [ Use the dot tool from graphviz to generate more advanced diagrams
                                and graphs ] ]]
    examples [[ description = [ Build doxyapp ] ]]
    qt5      [[ description = [ Build doxywizard ] ]]
"

# tests require latex (bibtex)
RESTRICT="test"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        dev-lang/python:*
        dev-scm/git
        sys-devel/bison[>=2.7]
        sys-devel/flex
        doc? ( virtual/unzip )
    build+run:
        app-text/ghostscript
        qt5? ( x11-libs/qtbase:5[gui] )
    run:
        dot? (
            media-gfx/graphviz[>=1.8.10]
            media-libs/freetype:2
        )
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/${PN}-1.8.14-doc.patch
    "${FILES}"/${PNV}-Fix-memory-corruption-in-TextStream.patch
)

CMAKE_SRC_CONFIGURE_PARAMS=(
    -Dbuild_doc:BOOL=TRUE
    -Dbuild_doc_chm:BOOL=FALSE
    -Dbuild_parse:BOOL=FALSE
    -Dbuild_search:BOOL=FALSE
    # Unpackaged (https://github.com/javacc/javacc), would recreate some files
    # for vhdlparser, but falls back to the ones shipped with the tarball.
    -DCMAKE_DISABLE_FIND_PACKAGE_Javacc:BOOL=FALSE
    # https://pypi.org/project/generateDS/, automatically recreates the XML
    # parser modules when updating the schema files
    -DCMAKE_DISABLE_FIND_PACKAGE_generateDS:BOOL=FALSE
    -Denable_coverage:BOOL=FALSE
    -Dforce_qt4:BOOL=FALSE
    -DSANITIZE_ADDRESS:BOOL=FALSE
    -DSANITIZE_MEMORY:BOOL=FALSE
    -DSANITIZE_THREAD:BOOL=FALSE
    -DSANITIZE_UNDEFINED:BOOL=FALSE
    -Duse_libclang:BOOL=FALSE
    -Duse_sqlite3:BOOL=FALSE
    # Only set if compiler is clang, however I can't easily test this
    -Duse_libc++:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTIONS=(
    'examples build_app'
    'examples build_xmlparser'
    'qt5 build_wizard'
)

DEFAULT_SRC_INSTALL_EXTRA_DOCS=( LANGUAGE.HOWTO )

src_compile() {
    default

    emake docs
}

src_test() {
    # UnicodeDecodeError on Python 3, last checked: 1.8.14
    require_utf8_locale

    default
}

src_install() {
    cmake_src_install

    if option doc; then
        insinto /usr/share/doc/${PNVR}
        doins "${WORKBASE}"/${PN}_manual-${PV}.pdf
    fi

    # we disable the search tools with build_search above
    edo rm "${IMAGE}"/usr/share/man/man1/doxy{indexer,search}.1

    option qt5 || edo rm "${IMAGE}"/usr/share/man/man1/doxywizard.1
}

