Title: Some systemd units are no longer enabled by default
Author: Steven Siloti <ssiloti@gmail.com>
Content-Type: text/plain
Posted: 2019-04-12
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: sys-apps/systemd

Prior to systemd 242, some units were enabled by default.
You now need to enable these manually if you want to use them.
The affected units are:

    getty@tty1.service
    systemd-networkd.service
    systemd-networkd.socket
    systemd-resolved.service
    remote-cryptsetup.target
    remote-fs.target
    systemd-networkd-wait-online.service
    systemd-timesyncd.service
